﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Text.RegularExpressions;
using System.IO;
namespace h1
{
    public partial class Form1 : Form
    {
        //private delegate void myDelegate();
        private string key;
        Thread thread;
        //private Hashtable result = new Hashtable();
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            thread = new Thread(display);
            thread.IsBackground = true;
            thread.Start();

        }
        private void display()
        {
            string keywords = key.Trim();
            string url = "https://baike.baidu.com/item/" + keywords;
            //string url = "https://www.baidu.com/baidu?wd=" + keywords;
            string result = search_Baidu(url);
            Control.CheckForIllegalCrossThreadCalls = false;
            Console.WriteLine(result);
            txt_result.Text = result;
        }


        private string search_Baidu(string url)
        {
            string Info = null;
            try
            {
                WebRequest myRequest = WebRequest.Create(url);
                WebResponse myResponse = myRequest.GetResponse();
                Stream resStream = myResponse.GetResponseStream();
                StreamReader sr = new StreamReader(resStream);
                Info = sr.ReadToEnd();
                //Info=Info.Substring(0, 199);
                sr.Close();
                myResponse.Close();

            }
            catch
            {

            }
            return Info;
        }

        private void txt_key_TextChanged(object sender, EventArgs e)
        {
            key = txt_key.Text;
        }
        

       
    }
}

